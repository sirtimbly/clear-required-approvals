const eslintConfig = {
  env: {
    es6: true,
    node: true,
  },
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:prettier/recommended",
    "prettier",
  ],
  plugins: ["@typescript-eslint", "prettier"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: 2021,
    sourceType: "module",
  },
  rules: {
    // Use "type" over "interface".
    // Types can do all interfaces can and are more flexible.
    "@typescript-eslint/consistent-type-definitions": ["error", "type"],
    // Allow the use of explicit any. Sometimes, typing something just isn't worth it.
    "@typescript-eslint/no-explicit-any": ["off"],
    // No double equals.
    eqeqeq: ["error", "always"],
    // Types must be in PascalCase.
    "@typescript-eslint/naming-convention": [
      "error",
      {
        selector: ["interface", "typeAlias"],
        format: ["PascalCase"],
        custom: {
          regex: "^I[A-Z]",
          match: false,
        },
      },
    ],
  },
};

module.exports = eslintConfig;
