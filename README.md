# clear-required-approvals

A small script meant to run alongside RenovateBot to clear any required approvals on any "automerge" Gitlab MRs.

## How To Use

1. Install `clear-required-approvals`

```bash
yarn add clear-required-approvals
# or
npm install clear-required-approvals
```

2. Ensure you have a proper RenovateBot environment.

   - Be sure you have `RENOVATE_CONFIG_FILE` available in your env.
   - Be sure you have `RENOVATE_TOKEN` available in your env.

3. (Optional) Set your target MR label.

   - By default, this script looks for MRs with the label "RenovateBot: Automerge". If you wish to target a different label, set `CLEAR_REQUIRED_APPROVALS_LABEL="<my automerge label>"`.

4. Wherever you run `./node_modules/.bin/renovate`, run `./node_modules/.bin/clear-required-approvals` before it.

## Contributing

Repo: <https://gitlab.com/blackarctic/clear-required-approvals>

Be sure to run `yarn run check` to ensure proper formatting and linting.

## Authors

Created by Nick Galloway
