import { env } from "../../env";
import { clearRequiredApprovalsByProject } from "./clearRequiredApprovalsByProject";

const { REPOSITORIES } = env;

export const clearRequiredApprovals = async (): Promise<void> => {
  const promises = REPOSITORIES.map((repoId) =>
    clearRequiredApprovalsByProject(encodeURIComponent(repoId))
  );

  await Promise.allSettled(promises);
};
