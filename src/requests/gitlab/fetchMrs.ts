import { Mr, ApprovalState, ApprovalRule } from "../../models/Mr";

import { executeGitlabReq } from "./helpers";

const fetchAllOpenMrIds = async (
  projectId: string,
  labels: string[]
): Promise<string[]> => {
  const response = await executeGitlabReq({
    projectId,
    params: {
      state: "opened",
      labels,
    },
  });
  const { data } = response;
  if (!data) {
    throw new Error("No data found for fetchAllOpenMrIds");
  }
  return data.map((x: { iid: string }) => x.iid);
};

const fetchMr = async (projectId: string, id: string): Promise<Mr> => {
  const response = await executeGitlabReq({
    projectId,
    params: {
      include_diverged_commits_count: true,
      include_rebase_in_progress: true,
    },
    path: `/${id}`,
  });
  const { data } = response;
  if (!data) {
    throw new Error(`No data found for fetchMr of id "${id}"`);
  }
  return data;
};

const fetchMrApprovalState = async (
  projectId: string,
  id: string
): Promise<ApprovalState> => {
  const response = await executeGitlabReq({
    projectId,
    path: `/${id}/approval_state`,
  });
  const { data } = response;
  if (!data) {
    throw new Error(`No data found for fetchMrApprovalState of id "${id}"`);
  }
  return data;
};

const fetchMrApprovalRules = async (
  projectId: string,
  id: string
): Promise<ApprovalRule[]> => {
  const response = await executeGitlabReq({
    projectId,
    path: `/${id}/approval_rules`,
  });
  const { data } = response;
  if (!data) {
    throw new Error(`No data found for fetchMrApprovalRules of id "${id}"`);
  }
  return data;
};

export const fetchAllOpenMrs = async (
  projectId: string,
  labels: string[]
): Promise<Mr[]> => {
  const allOpenMrIds = await fetchAllOpenMrIds(projectId, labels);
  const allOpenMrPromises = allOpenMrIds.map((id) => fetchMr(projectId, id));
  const allOpenMrApprovalStatePromises = allOpenMrIds.map((id) =>
    fetchMrApprovalState(projectId, id)
  );
  const allOpenMrApprovalRulesPromises = allOpenMrIds.map((id) =>
    fetchMrApprovalRules(projectId, id)
  );
  const allOpenMrs = await Promise.all(allOpenMrPromises);
  const allOpenMrApprovalState = await Promise.all(
    allOpenMrApprovalStatePromises
  );
  const allOpenMrApprovalRules = await Promise.all(
    allOpenMrApprovalRulesPromises
  );
  allOpenMrs.forEach((openMr, i) => {
    openMr.approval_state = allOpenMrApprovalState[i];
    openMr.approval_rules = allOpenMrApprovalRules[i];
  });
  return allOpenMrs;
};
