import { env } from "../../env";
import { fetchAllOpenMrs } from "./fetchMrs";
import { executeGitlabReq } from "./helpers";

const { CLEAR_REQUIRED_APPROVALS_LABEL } = env;

export const clearRequiredApprovalsByProject = async (
  projectId: string
): Promise<void> => {
  const allOpenMrs = await fetchAllOpenMrs(projectId, [
    CLEAR_REQUIRED_APPROVALS_LABEL,
  ]);

  const getDoesRuleNeedToBeCleared = (rule: {
    rule_type: string;
    approvals_required: number;
  }) => Boolean(rule.rule_type !== "code_owner" && rule.approvals_required);

  const mrs = allOpenMrs.filter(
    (mr) =>
      mr.approval_state.rules.some(getDoesRuleNeedToBeCleared) ||
      mr.approval_rules.some(getDoesRuleNeedToBeCleared)
  );

  const approvalsPromises = mrs.map((mr) =>
    executeGitlabReq({
      projectId,
      method: "post",
      path: `/${mr.iid}/approvals`,
      params: {
        approvals_required: 0,
      },
    })
  );

  const approvalRulesPromises = mrs.flatMap((mr) =>
    mr.approval_rules.map((approvalRule) =>
      executeGitlabReq({
        projectId,
        method: "put",
        path: `/${mr.iid}/approval_rules/${approvalRule.id}`,
        params: {
          approvals_required: 0,
        },
      })
    )
  );

  const approvePromises = mrs.map((mr) =>
    executeGitlabReq({
      projectId,
      method: "post",
      path: `/${mr.iid}/approve`,
    })
  );

  await Promise.allSettled([
    ...approvalsPromises,
    ...approvalRulesPromises,
    ...approvePromises,
  ]);

  const mrTitles = mrs.map((x) => x.title);
  if (mrTitles.length) {
    console.log(
      `✅ Cleared required approvals on ${
        mrTitles.length
      } MR(s) in project "${decodeURIComponent(projectId)}"`,
      mrTitles
    );
  }
};
