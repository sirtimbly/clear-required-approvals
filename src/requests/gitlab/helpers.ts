import axios, { AxiosResponse } from "axios";

import { env } from "../../env";
import { stringifyParams } from "../../utils";

const { ENDPOINT, RENOVATE_TOKEN } = env;

const GITLAB_API_URL = ENDPOINT;

const GITLAB_AUTH_PARAMS = {
  access_token: RENOVATE_TOKEN,
};

const createGitlabReqParams = (params: Record<string, any>) => ({
  ...params,
  ...GITLAB_AUTH_PARAMS,
});

export const executeGitlabReq = async ({
  projectId,
  params = {},
  path = "",
  method = "get",
}: {
  projectId: string;
  params?: Record<string, any>;
  path?: string;
  method?: "get" | "post" | "put" | "delete";
}): Promise<AxiosResponse<any>> => {
  const GITLAB_PROJECTS_URL = `${GITLAB_API_URL}/projects/${projectId}`;
  const GITLAB_MR_URL = `${GITLAB_PROJECTS_URL}/merge_requests`;

  const stringifiedParams = stringifyParams(createGitlabReqParams(params));
  const url = `${GITLAB_MR_URL}${path}?${stringifiedParams}`;

  if (method === "get") {
    return await axios.get(url);
  }
  if (method === "post") {
    return await axios.post(url);
  }
  if (method === "put") {
    return await axios.put(url);
  }
  if (method === "delete") {
    return await axios.delete(url);
  }
  throw new Error(
    `executeGitlabReq method "${method}" is not valid. Expected "get" or "put".`
  );
};
