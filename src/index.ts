#!/usr/bin/env node

import { env } from "./env";
import { clearRequiredApprovals } from "./requests/gitlab/clearRequiredApprovals";

if (env.DRY_RUN) {
  console.log("Skipping due to dry run.");
  process.exit(0);
}

clearRequiredApprovals();
