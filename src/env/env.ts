import { InjectedEnv, Env } from "./types";
import { resolveApp } from "../utils";

const injectedEnv: InjectedEnv = process.env;

if (!injectedEnv.RENOVATE_CONFIG_FILE) {
  throw new Error("Could not resolve RENOVATE_CONFIG_FILE");
}
if (!injectedEnv.RENOVATE_TOKEN) {
  throw new Error("Could not resolve RENOVATE_TOKEN");
}

let renovateConfig: any;
try {
  renovateConfig = require(resolveApp(injectedEnv.RENOVATE_CONFIG_FILE));
} catch (error) {
  // do nothing. handle in next check.
}

if (!renovateConfig) {
  throw new Error(
    `Could not resolve renovate config at "${injectedEnv.RENOVATE_CONFIG_FILE}"`
  );
}

/* Ensure RENOVATE_TOKEN */

const RENOVATE_TOKEN = injectedEnv.RENOVATE_TOKEN;

if (typeof RENOVATE_TOKEN !== "string" || !RENOVATE_TOKEN) {
  throw new Error(`RENOVATE_TOKEN must be set`);
}

/* Ensure ENDPOINT */

const _ENDPOINT = renovateConfig.endpoint;

if (typeof _ENDPOINT !== "string" || !_ENDPOINT) {
  throw new Error(`"endpoint" must be set in your renovate config`);
}

// Remove the trailing slash if it exists
const ENDPOINT = _ENDPOINT.replace(/\/$/, "");

/* Ensure DRY_RUN */

const DRY_RUN = renovateConfig.dryRun;

if (typeof DRY_RUN !== "boolean") {
  throw new Error(`"dryRun" must be set in your renovate config`);
}

/* Ensure PLATFORM */

const PLATFORM = renovateConfig.platform;

if (typeof PLATFORM !== "string" || !PLATFORM) {
  throw new Error(`"platform" must be set in your renovate config`);
}

if (PLATFORM !== "gitlab") {
  throw new Error(`Only the gitlab platform is currently supported`);
}

/* Ensure REPOSITORIES */

const REPOSITORIES = renovateConfig.repositories;

if (!Array.isArray(REPOSITORIES)) {
  throw new Error(`"repositories" must be set in your renovate config`);
}

if (!REPOSITORIES.every((x) => typeof x === "string")) {
  throw new Error(
    `"repositories" must be set in your renovate config to an array of strings`
  );
}

/* Ensure CLEAR_REQUIRED_APPROVALS_LABEL */

const { CLEAR_REQUIRED_APPROVALS_LABEL = "RenovateBot: Automerge" } =
  injectedEnv;

if (typeof CLEAR_REQUIRED_APPROVALS_LABEL !== "string" || !PLATFORM) {
  throw new Error(
    `CLEAR_REQUIRED_APPROVALS_LABEL must be set to a non-empty string`
  );
}

export const env: Env = {
  RENOVATE_TOKEN,
  ENDPOINT,
  DRY_RUN,
  PLATFORM,
  REPOSITORIES,
  CLEAR_REQUIRED_APPROVALS_LABEL,
};
