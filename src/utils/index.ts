import queryString from "query-string";

import fs from "fs";
import path from "path";

export const resolveApp = (relativePath: string): string => {
  const appDirectory = fs.realpathSync(process.cwd());
  return path.resolve(appDirectory, relativePath);
};

export const stringifyParams = (params: Record<string, string>): string =>
  queryString.stringify(params, {
    arrayFormat: "comma",
  });
